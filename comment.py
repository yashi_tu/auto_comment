import time
import numpy as np
import pandas as pd
import math
import getpass

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.alert import Alert


DRIVER_PATH = './chromedriver_win32/chromedriver.exe'
COURSE_PATH = './info/Course_ID.csv'
STUDENT_PATH = './info/Student_ID.csv'
COMMENT_PATH = './data/Comment.csv'
# url_list
ISTU_URL='https://istu3g.dc.tohoku.ac.jp/istu3g/'
Course_ID = 'https://istu3g.dc.tohoku.ac.jp/istu3g/ReportEval/listview?qsCourse=45976&qsLecture=96787&qsReport=4201&qsAssign=5240'




class Comment:
    def __init__(self):
        # open chrome driver
        self.driver = webdriver.Chrome(executable_path=DRIVER_PATH)
        wait = WebDriverWait(self.driver, 50)

    def login_istu(self, accid, pw):
        self.driver.get(ISTU_URL)
        if self._loadpage():
            self.driver.find_element_by_id('UsersMasterAccountCode').send_keys(accid)
            self.driver.find_element_by_id('UsersMasterPassword').send_keys(pw)
            time.sleep(3)
            self.driver.find_element_by_xpath('//*[@id="UsersMasterLoginForm"]/div[2]/div/input').click()
        if self._loadpage():
            self.driver.find_element_by_xpath('//*[@id="classes"]/div[2]/table/tbody/tr[4]/td[2]/table/tbody/tr/td[2]/a')
        time.sleep(1)

    def comment_insert(self, qsReport, qsAssign):
        s_info = pd.read_csv(STUDENT_PATH, index_col='S_ID')
        c_info = pd.read_csv(COMMENT_PATH)
        for st, com in zip(c_info['S_ID'],c_info['Comment']):
            qsUser = s_info['qsUser'][st]
            url = f'https://istu3g.dc.tohoku.ac.jp/istu3g/ReportEval/edit?qsCourse=45976&qsLecture=96787&qsReport={qsReport}&qsAssign={qsAssign}&qsUser={qsUser}'
            self.driver.get(url)
            self._loadpage()
            print(com)
            self.driver.find_element_by_xpath('//*[@id="evalComment"]').clear()
            if not pd.isnull(com):
                self.driver.find_element_by_xpath('//*[@id="evalComment"]').send_keys(com)
            else:
                self.driver.find_element_by_xpath('//*[@id="evalComment"]').send_keys('')
            # has to change 一時保存をクリックする
            self.driver.find_element_by_xpath('//*[@id="btnRegLower"]').click()
            self._loadpage()
            Alert(self.driver).accept()
            self._loadpage()
        time.sleep(5)
        self.driver.quit()

    def create_qsUser(self):
        
        d = pd.read_csv('./info/Student_ID.csv')
        # //*[@id="SearchItemValidationListviewForm"]/section[2]/table/tbody/tr[1]/td[2]/a
        # //*[@id="SearchItemValidationListviewForm"]/section[2]/table/tbody/tr[2]/td[2]/a
        # //*[@id="SearchItemValidationListviewForm"]/section[2]/table/tbody/tr[126]/td[2]/a
        qs_User_list = []
        # loop
        for i,_ in enumerate(d['S_ID']):
            self.driver.get(Course_URL)
            self._loadpage()
            self.driver.find_element_by_xpath(f'//*[@id="SearchItemValidationListviewForm"]/section[2]/table/tbody/tr[{i+1}]/td[2]/a').click()
            self._loadpage()
            url = self.driver.current_url
            qsUser = int(url.replace('https://istu3g.dc.tohoku.ac.jp/istu3g/ReportEval/edit?qsCourse=45976&qsLecture=96787&qsReport=4201&qsAssign=5240&qsUser=',''))
            print(qsUser)
            qs_User_list.append(qsUser)
        d['qsUser'] = qs_User_list
        d.to_csv('./info/Student_ID.csv')

    def _loadpage(self):
        try:
            element = wait.until(EC.presence_of_all_elements_located)
            time.sleep(1)
        finally:
            return True





def main():
    # ログイン用パスワード取得
    pswd = getpass.getpass('Pasword:')

    # どの課題を採点するか選択
    course = pd.read_csv(COURSE_PATH)
    print('Which report?')
    for i, name in enumerate(course['Report']):
        print(f'{i}: {name}')
    r = int(input('NO:'))



    test = Comment()
    #ログイン用IDはここに入れてね:UserID
    ID = '?????'
    test.login_istu(ID,pswd)
    
    test.comment_insert(course['qsReport'][r],course['asAssign'][r])



if __name__ == '__main__':
    main()