# Auto_Comment

1. comment ファイルのmain関数：ID変数に自分のISTUログインIDを入力
2. data にComment.csv, info にStudent_ID.csv, Course_ID.csvを置く．(セキュリティのつ業上gitlabには載せないので樺島からもらう)
3. python3 comment.pyで実行　chromeが立ち上がり自動でコメント入力を行う．

- インストールがいるもの：
	- selenium (pip3 install selenium で入る)
	- pandas, numpy
    - chrome driver(ウェブから取ってくる)デフォで入ってるのはwindows用それ以外の人は別途ダウンロードする. macの場合はbrew install chromedriverで入るらしい

- info:
	- 受講生一覧と課題一覧が入っている．課題は追加されるごと，URLからIDを判別しなければならないので留意する．

- data:
	- コメントを書くcsvを置く．コメントがない場合は何も書かない，またはその学籍番号を削除しても構わない．
- 注意：
	- プログラミング上では絶対にログインパスワードを書かないこと．それから学校以外では使用できません．(統合認証システムに対応できないため)

	- chromedriverフォルダはいじらいない．

	- 新年度では学籍番号とURLIDの対応付けをして受講生一覧を更新する必要がある．Commentクラスのcreate_qsUserを実行することで対応できる．（参照URLを変更する必要はあると思う）※2018年度はすでに行ったため必要なし
	